
//=======================================================================
// Projectlist
//=======================================================================

models.ProjectListItem = Backbone.Model.extend({
    defaults : {
//        _id: "",
//        name: "name",
//        readme_url: "readme_url",
        success: "false",
        attributes: "",
    },
    initialize: function() {
        console.log("Initialized ProjectListItem")
    },
    // Example of how to do a validation in a model
    validate: function(attributes) {
//        if (typeof attributes.success !== 'string') {
//            // Return a failed validation
//            return 'Success is mandatory';
//        }
//        // All validations passed, don't return anything
//        console.log("Validated")
    }

});

models.ProjectListItems = Backbone.PageableCollection.extend({
    model: models.ProjectListItem,
    url: projectListRoot,
    initialize: function(options) {
        this.url= options.host;
    },
    parse: function (data) {
        console.log("Response Data : ")
        console.log(data)
//        console.log("Response Data : ")
//        console.log(data.success)
        return data;
    },
    state: {pageSize: 100},
    mode: "client" // page entirely on the client side
});


        //====================== Attributes ======================
        //{
        //    "_links": {
        //        "events": "http://localhost/gitlab/api/v4/projects/1/events",
        //        "issues": "http://localhost/gitlab/api/v4/projects/1/issues",
        //        "labels": "http://localhost/gitlab/api/v4/projects/1/labels",
        //        "members": "http://localhost/gitlab/api/v4/projects/1/members",
        //        "merge_requests": "http://localhost/gitlab/api/v4/projects/1/merge_requests",
        //        "repo_branches": "http://localhost/gitlab/api/v4/projects/1/repository/branches",
        //        "self": "http://localhost/gitlab/api/v4/projects/1"
        //    },
        //    "archived": false,
        //    "avatar_url": null,
        //    "ci_config_path": null,
        //    "container_registry_enabled": true,
        //    "created_at": "2018-08-27T06:27:49.792Z",
        //    "creator_id": 1,
        //    "default_branch": "master",
        //    "description": "",
        //    "forks_count": 0,
        //    "http_url_to_repo": "http://localhost/gitlab/gantry/web.git",
        //    "id": 1,
        //    "import_error": null,
        //    "import_status": "none",
        //    "issues_enabled": true,
        //    "jobs_enabled": true,
        //    "last_activity_at": "2018-08-27T18:45:49.536Z",
        //    "lfs_enabled": true,
        //    "merge_method": "merge",
        //    "merge_requests_enabled": true,
        //    "name": "web",
        //    "name_with_namespace": "gantry / web",
        //    "namespace": {
        //        "full_path": "gantry",
        //        "id": 2,
        //        "kind": "group",
        //        "name": "gantry",
        //        "parent_id": null,
        //        "path": "gantry"
        //    },
        //    "only_allow_merge_if_all_discussions_are_resolved": false,
        //    "only_allow_merge_if_pipeline_succeeds": false,
        //    "open_issues_count": 2,
        //    "path": "web",
        //    "path_with_namespace": "gantry/web",
        //    "permissions": {
        //        "group_access": {
        //            "access_level": 50,
        //            "notification_level": 3
        //        },
        //        "project_access": null
        //    },
        //    "printing_merge_request_link_enabled": true,
        //    "public_jobs": true,
        //    "readme_url": "http://localhost/gitlab/gantry/web/blob/master/README.md",
        //    "repository_storage": "default",
        //    "request_access_enabled": false,
        //    "resolve_outdated_diff_discussions": false,
        //    "runners_token": "F_aApA_bTk7vMpzXBrQ1",
        //    "shared_runners_enabled": true,
        //    "shared_with_groups": [],
        //    "snippets_enabled": true,
        //    "ssh_url_to_repo": "git@localhost:gantry/web.git",
        //    "star_count": 0,
        //    "tag_list": [],
        //    "visibility": "public",
        //    "web_url": "http://localhost/gitlab/gantry/web",
        //    "wiki_enabled": true
        //}


