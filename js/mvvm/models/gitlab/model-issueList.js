
//=======================================================================
// IssueList
//=======================================================================

models.IssueListItem = Backbone.Model.extend({
    defaults : {
//        _id: "",
        success: "false",
        issues: "",
    },
    initialize: function() {
        console.log("Initialized IssueListItem")
    },
    // Example of how to do a validation in a model
    validate: function(attributes) {
//        if (typeof attributes.success !== 'string') {
//            // Return a failed validation
//            return 'Success is mandatory';
//        }
//        // All validations passed, don't return anything
//        console.log("Validated")
    }

});

models.IssueListItems = Backbone.PageableCollection.extend({
    model: models.IssueListItem,
    url: issueListRoot,
    initialize: function(options) {
        this.url= options.host;
    },
    parse: function (data) {
        console.log("Response Data : ")
        console.log(data)
        console.log("Response Data : ")
        console.log(data.success)
        return data;
    },
    state: {pageSize: 100},
    mode: "client" // page entirely on the client side
});



//{
//    "issues": {
//        "issues": [
//            {
//                "assignee": {
//                    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//                    "id": 1,
//                    "name": "Administrator",
//                    "state": "active",
//                    "username": "root",
//                    "web_url": "http://localhost/gitlab/root"
//                },
//                "assignees": [
//                    {
//                        "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//                        "id": 1,
//                        "name": "Administrator",
//                        "state": "active",
//                        "username": "root",
//                        "web_url": "http://localhost/gitlab/root"
//                    }
//                ],
//                "author": {
//                    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
//                    "id": 1,
//                    "name": "Administrator",
//                    "state": "active",
//                    "username": "root",
//                    "web_url": "http://localhost/gitlab/root"
//                },
//                "closed_at": null,
//                "closed_by": null,
//                "confidential": false,
//                "created_at": "2018-08-27T06:53:37.597Z",
//                "description": "",
//                "discussion_locked": null,
//                "downvotes": 0,
//                "due_date": "2018-08-31",
//                "id": 2,
//                "iid": 2,
//                "labels": [
//                    "To Do"
//                ],
//                "milestone": {
//                    "created_at": "2018-08-27T06:54:25.649Z",
//                    "description": "",
//                    "due_date": "2018-08-31",
//                    "id": 2,
//                    "iid": 1,
//                    "project_id": 1,
//                    "start_date": "2018-08-27",
//                    "state": "active",
//                    "title": "Initial Milestone",
//                    "updated_at": "2018-08-27T06:54:25.649Z",
//                    "web_url": "http://localhost/gitlab/gantry/web/milestones/1"
//                },
//                "project_id": 1,
//                "state": "opened",
//                "time_stats": {
//                    "human_time_estimate": null,
//                    "human_total_time_spent": null,
//                    "time_estimate": 0,
//                    "total_time_spent": 0
//                },
//                "title": "Test Issue 2",
//                "updated_at": "2018-08-27T06:55:52.239Z",
//                "upvotes": 0,
//                "user_notes_count": 0,
//                "web_url": "http://localhost/gitlab/gantry/web/issues/2"
//            }
//        ]
//    },
//    "success": {
//        "success": true
//    },
//    "test": "test message"
//}