
//=======================================================================
// ProxyList
//=======================================================================

models.ProxyListItem = Backbone.Model.extend({
    defaults : {
//        _id: "",
        success: "false",
        proxys: "",
    },
    initialize: function() {
        console.log("Initialized ProxyListItem")
    },
    // Example of how to do a validation in a model
    validate: function(attributes) {
//        if (typeof attributes.success !== 'string') {
//            // Return a failed validation
//            return 'Success is mandatory';
//        }
//        // All validations passed, don't return anything
//        console.log("Validated")
    }

});

models.ProxyListItems = Backbone.PageableCollection.extend({
    model: models.ProxyListItem,
    url: null,
    initialize: function(options) {
        this.url= options.host;
    },
    parse: function (data) {
        console.log("Response Data : ")
        console.log(data)
        return data;
    },
    state: {pageSize: 100},
    mode: "client" // page entirely on the client side
});

