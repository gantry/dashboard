// ============================================
// ProxyList View
// ============================================

views.ProxyList = Backbone.View.extend({

    tagName: 'tbody',

    initialize: function(options) {
        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("ProxyList render : ");

        // Clear
        jQuery(this.el).empty();
        jQuery("#proxyList").empty();

        // Add Table Header
        jQuery("#proxyList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue">Name</th>'
            + '<th class="tabblue">Source</th>'
            + '<th class="tabblue">Destination</th>'
            + '</tr>'
            + '</thead>'
        ));

        proxyList = this.model.get('proxyList').proxies;
        console.log("ProxyList");
        console.log(proxyList);
        for (var i = 0; i < proxyList.length; i++)
        {
            proxy = proxyList[i];

            console.log("Printing Proxy=======")
            console.log(proxy);

            jQuery(this.el).append(jQuery(
                '<tr class="serviceItem"'

//                BUG : Not sure why we have to add an extra row to make the other columns show up here
                + '<td>'+ proxy.source  + '</td>'
                + '<td>'+ proxy.name + '</td>'
                + '<td>'+ proxy.source  + '</td>'
                + '<td>'+ proxy.destination  + '</td>'
                + '</tr>'
            ));

            var rowName = proxy.name + '-ContainerDetail';
            console.log("================RowName==============");
            console.log(rowName);
            var row = jQuery(rowName);
            console.log(row);
            row.append(jQuery(
                '<div class="small-9 medium-9 large-9 cell serviceDiv">'
                + '    <table id="" >'
                + '      <thead>'
                + '        <tr>'
                + '          <th class="tabblue">Name</th>'
                + '          <th class="tabblue">Source</th>'
                + '          <th class="tabblue">Destination</th>'
                + '        </tr>'
                + '      </thead>'
                + '      <tbody>'
                + '        <tr>'
                + '         <td>'+ proxy.source  + '</td>'
                + '         <td>'+ proxy.name + '</td>'
                + '         <td>'+ proxy.source  + '</td>'
                + '         <td>'+ proxy.destination  + '</td>'
                + '        </tr>'
                + '      </tbody>'
                + '    </table>'
                + '</div>'

            ));

        }

        return this;
    }
});


views.ProxyListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    el: '#proxyList',

    initialize: function(options){
        this.collection = options.collection;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);

    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("ProxyList");
        console.log(this.collection);

        // Go through the collection items
        this.collection.forEach(function(item) {
            //alert("here: " + item);

            var itemView = new views.ProxyList({
                model: item
            });
            console.log("ProxyListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);
        });

        //this.$el.html(this.template({
        //	who: "Fred",
        //  collection: this.collection
        //}));

        return this;
    }
});

