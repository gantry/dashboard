


function computeCommand(data)
{    $.ajax({
             type: "POST",
             url: lxdListRoot,
             data: JSON.stringify(data),// now data come in this function
             contentType: "application/json; charset=utf-8",
             crossDomain: true,
             dataType: "json",
             success: function (data, status, jqXHR) {

                 alert("success");// write success in " "
             },

             error: function (jqXHR, status) {
                 // error handler
                 console.log(jqXHR);
                 alert('fail' + status.code);
             }
          });
    }



// ============================================
// LxdList View
// ============================================

views.LxdList = Backbone.View.extend({

    tagName: 'tbody',
    firewall: null,

    initialize: function(options) {

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

//        console.log("===================Firewall===========================");
//        // console.log("Creating ProxyList Model")
//        this.firewall = new models.ProxyListItems({ host: proxyListRoot });
//        this.firewall.fetch();

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("LxdList render : ");



//        // ==========================================================
//        // Render Proxy Elements
//        // Pass in the element name the list should be attached to.
//        // Also, filter firewall list based on Container Name.
//        // console.log("Rendering ProxyList View")
//        console.log("================Firewall !!==============================");
//        var item = this.firewall.model;
//        console.log(item);
//        for (var i = 0; i < item.length; i++)
//        {
//            var proxy = item.get(i).model.get("proxies");
//            console.log(proxy);
//        }
//        this.firewall.each(function(proxy) {
//            console.log('Proxy item.', proxy.name);
//        });
//        var proxyListView = new views.ProxyListView({
//            collection : this.firewall,
//        });

        // ==========================================================
        proxyList = this.model.get('proxyList').proxies;
        console.log("Proxy List");
        console.log(proxyList);
        for (var i = 0; i < proxyList.length; i++)
        {
            proxy = proxyList[i];
            console.log(proxy);
            jQuery("#proxyList").append(jQuery(
                '<tr class="serviceItem">'
                + '<td><a href="#" class=""><div>'+ proxy + '</div></a></td>'
                + '</tr>'
            ));
        }



        // Clear
        jQuery(this.el).empty();
        jQuery("#lxdList").empty();


        // Add Table Header
        jQuery("#lxdList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue serviceHeader">Name</th>'
            + '<th class="tabblue serviceHeader">Status</th>'
            + '<th class="tabblue serviceHeader">Ip Address</th>'
            + '<th class="tabblue serviceHeader">Controls</th>'
            + '<th class="tabblue serviceHeader">Status</th>'
            + '</tr>'
            + '</thead>'
        ));

        // LXD Compute List
        lxdList = this.model.get('containers_all')
        console.log("LxdList");
        console.log(lxdList);
        for (var i = 0; i < lxdList.length; i++)
        {
            containerList= lxdList[i].containers;
            for (var j = 0; j < containerList.length; j++)
            {
                container = containerList[j];
                console.log("================ContainerName==============");
                console.log(container.name);

                var response =
                '<tr class="table-expand-row dataOpenDetails">'
                    + '<td>'+ container["name"] + '</td>'
                    + '<td>'+ container["settings"].flags  + '</td>'
                    + '<td><a href="http://'+ container["settings"].ipv4  + '"><div class="serviceLink">'+ container["settings"].ipv4  + '</div></a></td>'
                    + '<td>'
                    + '     <button onclick=\'computeCommand("play")\'><i class="fa fa-play fa-2x buttonSpacer" aria-hidden="true"></i></button>'
                    + '     <button onclick=\'computeCommand("stop")\'><i class="fa fa-stop fa-2x buttonSpacer" aria-hidden="true"></i></button>'
                    + '     <button onclick=\'computeCommand("delete")\'><i class="fa fa-trash fa-2x buttonSpacer" aria-hidden="true"></i></button>'
                    + '</td>'
                    + '<td>in progress <span class="expand-icon"></span></td>'
                    + '</tr>'
                    + '<tr class="table-expand-row-content">'
                    + '<td id="'+ container["name"] +'-ContainerDetail" colspan="8" class="table-expand-row-nested">'

                    + '<b>Firewall</b>'
                    + '<table class="compact">'

                    + '<thead>'
                    + '<tr class="table-expand-row">'
                    + '<th class="">Name</th>'
                    + '<th class="">Source</th>'
                    + '<th class="">Destination</th>'
                    + '</tr>'
                    + '</thead>'

                    + '<tbody>'

                for (var k = 0; k < proxyList.length; k++)
                {
                    response +=
                        '<tr class="table-expand-row">'
                        + '<td>'+ proxyList[k].name + '</td>'
                        + '<td>'+ proxyList[k].source  + '</td>'
                        + '<td>'+ proxyList[k].destination  + '</td>'
                        + '</tr>'
                }


                response +=
                    '</tbody>'
                    + '</table>'
                    + '</td>'
                    + '</tr>'

                // Render
                jQuery(this.el).append(jQuery(response));



            }
            lxd = lxdList[i];
            console.log(lxd);


        }

        // Bind MasterRow OnClick Event to the DetailRow "Toggle Method"
        jQuery(this.el).append(jQuery(
            '<div>'
            + '<script>'
            + '		$(".dataOpenDetails").bind("click", { key1: "value1", key2: "value2" }, dataOpenDetails);'
            + '</script>'
            + '</div>'
        ));


        // Playbooks
        console.log("==============================================");
        jQuery("#playbookList").empty();

        // Add Table Header
        jQuery("#playbookList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue">Playbooks</th>'
            + '</tr>'
            + '</thead>'
        ));

        // Playbook List
        playbookList = this.model.get('playbooks')
        console.log("Playbook List");
        console.log(playbookList);
        for (var i = 0; i < playbookList.length; i++)
        {
            playbook = playbookList[i];
            console.log(playbook);
            jQuery("#playbookList").append(jQuery(
                '<tr class="tabblue">'
                + '<td><a href="#" class=""><div>'+ playbook + '</div></a></td>'
                + '</tr>'
            ));
        }


        // Playbook List for "New Container" Button
        console.log("==============================================");
        for (var i = 0; i < playbookList.length; i++)
        {
            playbook = playbookList[i];
            console.log("PlaybookListDropDown");
            console.log(playbook);
            jQuery(".playbookListDropDown").append(jQuery(
                '<option value="'+ playbook + '">'
                + playbook + '</option>'
            ));
        }



        return this;
    }
});


views.LxdListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    firewall: null,
    el: '#lxdList',

    initialize: function(options){
        this.collection = options.collection;
        this.firewall = options.firewall;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);

    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("LxdList");
        console.log(this.collection);


        // Go through the collection items
        this.collection.forEach(function(item) {

            var itemView = new views.LxdList({
                model: item
            });
            console.log("LxdListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);

        });




//        element.append(jQuery(
//            '<div>'
//            + '<script>$(document).foundation();</script>'
//            + '</div>'
//        ));

        return this;
    }
});

