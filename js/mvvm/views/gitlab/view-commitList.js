// ============================================
// CommitList View
// ============================================

views.CommitList = Backbone.View.extend({

    tagName: 'tbody',

    initialize: function(options) {
        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("CommitList render : ");

        // Clear
        jQuery(this.el).empty();
        jQuery("#commitList").empty();

        // Add Table Header
        jQuery("#commitList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue">Title</th>'
            + '<th class="tabblue">Message</th>'
            + '<th class="tabblue">Project Id</th>'
            + '<th class="tabblue">Author Name</th>'
            + '<th class="tabblue">Author Email</th>'
            + '<th class="tabblue">Authored Date</th>'
            + '<th class="tabblue">Committer Email</th>'
            + '<th class="tabblue">Committer Name</th>'
            + '<th class="tabblue">Created At</th>'
//            + '<th class="tabblue">Id</th>'
//            + '<th class="tabblue">Parent Ids</th>'
            + '<th class="tabblue">Short Id</th>'
            + '</tr>'
            + '</thead>'
        ));


        commitList = this.model.get('commits').commits;
        console.log("CommitList");
        console.log(commitList);

        if(commitList){
            for (var i = 0; i < commitList.length; i++)
            {
                commit = commitList[i];
                console.log(commit);

                jQuery(this.el).append(jQuery(
                    '<tr class="serviceItem">'
                    + '<td>'+ commit["title"]  + '</td>'
                    + '<td>'+ commit["message"]  + '</td>'
                    + '<td>'+ commit["project_id"]  + '</td>'
                    + '<td>'+ commit["author_name"]  + '</td>'
                    + '<td>'+ commit["author_email"]  + '</td>'
                    + '<td>'+ commit["authored_date"]  + '</td>'
                    + '<td>'+ commit["committer_email"]  + '</td>'
                    + '<td>'+ commit["committer_name"]  + '</td>'
                    + '<td>'+ commit["created_at"]  + '</td>'
        //                + '<td>'+ commit["id"]  + '</td>'
        //                + '<td>'+ commit["parent_ids"]  + '</td>'
                    + '<td>'+ commit["short_id"]  + '</td>'

                    + '</tr>'
                ));

            }
        }
        return this;
    }
});


views.CommitListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    el: '#commitList',

    initialize: function(options){
        this.collection = options.collection;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);

    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("CommitList");
        console.log(this.collection);

        // Go through the collection items
        this.collection.forEach(function(item) {
            //alert("here: " + item);

            var itemView = new views.CommitList({
                model: item
            });
            console.log("CommitListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);
        });

        //this.$el.html(this.template({
        //	who: "Fred",
        //  collection: this.collection
        //}));

        return this;
    }
});

