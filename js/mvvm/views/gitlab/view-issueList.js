// ============================================
// IssueList View
// ============================================

views.IssueList = Backbone.View.extend({

    tagName: 'tbody',

    initialize: function(options) {
        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("IssueList render : ");

        // Clear
        jQuery(this.el).empty();
        jQuery("#issueList").empty();

        // Add Table Header
        jQuery("#issueList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue">Title</th>'
//            + '<th class="tabblue">Milestone</th>'
            + '<th class="tabblue">State</th>'
            + '<th class="tabblue">Web</th>'
            + '<th class="tabblue">Created</th>'
            + '<th class="tabblue">Updated</th>'
            + '<th class="tabblue">Message</th>'
            + '<th class="tabblue">Due</th>'
            + '</tr>'
            + '</thead>'
        ));

        issueList = this.model.get('issues').issues;
        console.log("IssueList");
        console.log(issueList);

        if(issueList){
            for (var i = 0; i < issueList.length; i++)
            {
                issue = issueList[i];
                console.log(issue);
                jQuery(this.el).append(jQuery(
                    '<tr class="serviceItem">'
                    + '<td>'+ issue["title"]  + '</td>'
//                    + '<td>'+ issue["milestone"].title  + '</td>'
                    + '<td>'+ issue["state"]  + '</td>'
                    + '<td>'+ issue["web_url"]  + '</td>'
                    + '<td>'+ issue["created_at"]  + '</td>'
                    + '<td>'+ issue["updated_at"]  + '</td>'
                    + '<td>'+ issue["message"]  + '</td>'
                    + '<td>'+ issue["due_date"]  + '</td>'

                    + '</tr>'
                ));
            }
        }
        return this;
    }
});


views.IssueListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    el: '#issueList',

    initialize: function(options){
        this.collection = options.collection;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);
    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("IssueList");
        console.log(this.collection);

        // Go through the collection items
        this.collection.forEach(function(item) {
            //alert("here: " + item);

            var itemView = new views.IssueList({
                model: item
            });
            console.log("IssueListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);
        });

        //this.$el.html(this.template({
        //	who: "Fred",
        //  collection: this.collection
        //}));

        return this;
    }
});

